//
//  SpecSchemaDef.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public class SpecDef: Codable {
    let type: String
    
    init(type: String) {
        self.type = type
    }
}

public class SpecSchemaDef: SpecDef {
    let properties: [String: SpecProperty]
    let required: [String]
    
    enum CodingKeys: String, CodingKey {
        case type
        case properties
        case required
    }
    
    init(
        type: String,
        properties: [String: SpecProperty],
        required: [String]
    ) {
        self.properties = properties
        self.required = required
        super.init(type: type)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let type = try container.decode(String.self, forKey: .type)
        
        self.properties = try container.decode([String: SpecProperty].self, forKey: .properties)
        self.required = try container.decode([String].self, forKey: .required)
                    
        super.init(type: type)
    }
    
    public override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
                
        try container.encode(self.type, forKey: .type)
        try container.encode(self.properties, forKey: .properties)
        if self.required.count > 0 {
            try container.encode(self.required, forKey: .required)
        }
        
    }
}

public class SpecEnumDef: SpecDef {
    let `enum`: [String]
    
    enum CodingKeys: String, CodingKey {
        case type
        case `enum`
    }
    
    init(
        type: String,
        enum: [String]
    ) {
        self.enum = `enum`
        super.init(type: type)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let type = try container.decode(String.self, forKey: .type)
          
        self.enum = try container.decode([String].self, forKey: .enum)
          
        super.init(type: type)
    }
      
    public override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
                  
        try container.encode(self.type, forKey: .type)
        try container.encode(self.enum, forKey: .enum)        
    }
}
