//
//  SpecOperationSecurity.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public enum OperationSecurity {
    case jwt(String)
}

public extension OperationSecurity {
    enum CodingKeys: String, CodingKey {
        case auth_jwt
    }
}



