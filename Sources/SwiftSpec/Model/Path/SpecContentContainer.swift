//
//  SpecContent.swift
//  SwiftSpec
//
//  Created by iLem0n on 25.07.20.
//

import Foundation

public struct SpecContentContainer: Codable {
    public private(set) var json: SpecContent? = nil
    public private(set) var file: SpecContent? = nil
    
    enum CodingKeys: String, CodingKey {
        case json = "application/json"
        case file = "multipart/octet-stream"
    }
    
    public init(json: SpecContent?) {
        self.json = json
    }
    
    public init(file: SpecContent?) {
        self.file = file
    }
}

public enum SpecContent {
    case schema(SpecProperty)
}

extension SpecContent: Codable {
    enum CodingKeys: String, CodingKey {
        case schema
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let property = try container.decode(SpecProperty.self, forKey: .schema)
        self = .schema(property)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        switch self {
        case .schema(let property):
            try container.encode(property, forKey: .schema)
        }
    }
}
