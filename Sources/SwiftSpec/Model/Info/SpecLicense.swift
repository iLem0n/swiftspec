//
//  SpecLicense.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public struct SpecLicense: Codable {
    public var name: String
    public var url: URL
      
    public init(name: String, url: URL) {
        self.name = name
        self.url = url
    }
}
