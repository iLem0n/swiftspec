//
//  SpecPath.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public struct SpecPath: Codable {
    public var parameters: [SpecParameter]?
    public var post: SpecOperation?
    public var get: SpecOperation?
    public var patch: SpecOperation?
    public var delete: SpecOperation?
    
    public init(
        parameters: [SpecParameter]? = nil,
        post: SpecOperation? = nil,
        get: SpecOperation? = nil,
        patch: SpecOperation? = nil,
        delete: SpecOperation? = nil
    ) {
        self.parameters = parameters
        self.post = post
        self.get = get
        self.patch = patch
        self.delete = delete
    }
}
