//
//  SpecComponents.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public struct SpecComponents: Codable {
    public var schemas: [String: SpecDef] = [:]
    public var securitySchemes: [String: SpecSecuritySchema] = [:]
    
    mutating public func add(_ schema: SpecSchemaDef, forKey schemaKey: String) throws {
        guard !self.schemas.keys.contains(schemaKey) else {
            throw SpecError.duplicateSchemaKey(schemaKey)
        }
                
        self.schemas[schemaKey] = schema
    }
    
    mutating public func add(_ enum: SpecEnumDef, forKey schemaKey: String) throws {
        guard !self.schemas.keys.contains(schemaKey) else {
            throw SpecError.duplicateSchemaKey(schemaKey)
        }
                
        self.schemas[schemaKey] = `enum`
    }
       
    mutating public func addSecurityScheme(_ securityScheme: SpecSecuritySchema, forKey securitySchemaKey: String) throws {
        guard !self.securitySchemes.keys.contains(securitySchemaKey) else {
            throw SpecError.duplicateSecuritySchemeKey(securitySchemaKey)
        }
           
        self.securitySchemes[securitySchemaKey] = securityScheme
    }
}
