//
//  Where.swift
//  App
//
//  Created by iLem0n on 20.06.20.
//

import Foundation

public struct Where: Codable {
    var stringValue: String?
    var doubleValue: Double?
    var intValue: Int?
    var boolValue: Bool?
    var dictValue: Dictionary<String, Where>?
    var arrayValue: Array<Where>?
    
    public init(from decoder: Decoder) throws {
        self.stringValue = try? decoder.singleValueContainer().decode(String.self)
        self.doubleValue = try? decoder.singleValueContainer().decode(Double.self)
        self.intValue = try? decoder.singleValueContainer().decode(Int.self)
        self.boolValue = try? decoder.singleValueContainer().decode(Bool.self)
        self.dictValue = try? decoder.singleValueContainer().decode(Dictionary<String, Where>.self)
        self.arrayValue = try? decoder.singleValueContainer().decode(Array<Where>.self)
    }
   
    var leafAsString: String? {
        guard self.isLeaf == true else { return nil }
        
        var value: String? = nil
        if let string = self.stringValue {
            value = string
        } else if let double = self.doubleValue {
            value = "\(double)"
        } else if let int = self.intValue {
            value = "\(int)"
        } else if let bool = self.boolValue {
            value = "\(bool)"
        }
        
        return value
    }
    
    var isLeaf: Bool {
        return self.stringValue != nil
            || self.doubleValue != nil
            || self.intValue != nil
            || self.boolValue != nil
    }
}
