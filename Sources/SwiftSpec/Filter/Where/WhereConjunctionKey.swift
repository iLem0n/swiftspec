//
//  WhereConjunctionKey.swift
//  App
//
//  Created by iLem0n on 11.06.20.
//

import Foundation

enum WhereConjunctionKey: String {
    case and
    case or
}
