//
//  SpecEntity.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public protocol SpecEnum: Codable {
    static var schemaType: String { get }
    static var schemaName: String { get }
    static var schema: SpecEnumDef { get }
}

public extension SpecEnum where Self: CaseIterable & RawRepresentable {
    static var schemaType: String { "string" }
    static var schemaName: String { "\(self)" }
    static var schema: SpecEnumDef {
        .init(
            type: self.schemaType,
            enum: self.allCases.map({ "\($0.rawValue)" })
        )
    }
}


public protocol SpecEntity: Codable {
    static var schema: SpecSchemaDef { get }
    static var schemaName: String { get }
    static var schemaType: String { get }
    static var schemaProperties: [String: SpecProperty] { get }
    static var schemaRequiredPropertyNames: [String] { get }
}

public extension SpecEntity {
    static var schemaType: String { "object" }
    static var schemaName: String { "\(self)" }
    static var schema: SpecSchemaDef {
        .init(
            type: self.schemaType,
            properties: self.schemaProperties,
            required: self.schemaRequiredPropertyNames
        )
    }
    
    static var schemaRequiredPropertyNames: [String] {
        self.schemaProperties.filter { (key: String, value: SpecProperty) -> Bool in
            switch value {
            case .ref(_, let req),
                 .refString(_, let req),
                 .int(_, let req),
                 .double(_, let req),
                 .uuid(let req),
                 .bool(let req),
                 .array(_, let req),
                 .string(_, let req),
                 .file(let req):
                return req;
            }
        }
        .map {
            $0.key
        }
    }
}
