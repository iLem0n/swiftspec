//
//  WhereNode.swift
//  App
//
//  Created by iLem0n on 11.06.20.
//

import Foundation

enum WhereNode {
    case branch(Where, String? = nil)
    case and(Array<Where>)
    case or(Array<Where>)
}


