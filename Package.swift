// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftSpec",
    products: [
        .library(name: "SwiftSpec",targets: ["SwiftSpec"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(name: "SwiftSpec", dependencies: []),
        .testTarget(name: "SwiftSpecTests", dependencies: ["SwiftSpec"]),
    ]
)
