//
//  Filter.swift
//  App
//
//  Created by iLem0n on 20.06.20.
//

import Foundation

public struct Filter: Codable {
    public var `where`: Where?
    public var sort: Sorting?
    public var limit: Int?
    public var offset: Int?
    
    init(`where`: Where, sort: Sorting?, limit: Int?, offset: Int?) {
        self.where = `where`
        self.sort = sort
        self.limit = limit
        self.offset = offset
    }
}
