//
//  SpecOperation.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public struct SpecOperation: Codable {
    public let operationId: String
    public let summary: String
    public let description: String
    public let tags: [String]
    public let responses: [String: SpecResponse]
    public let parameters: [SpecParameter]?
    public private(set) var security: [[String: Array<String>]]? = nil
    public let deprecated: Bool?
    public let requestBody: SpecRequestBody?

    public init(
        operationId: String,
        summary: String,
        description: String,
        controllerType: SpecController.Type,
        responses: [String: SpecResponse],
        requestBody: SpecRequestBody? = nil,
        parameters: [SpecParameter]? = nil,
        security: String? = nil,
        deprecated: Bool? = nil
    ) {
        self.operationId = operationId
        self.summary = summary
        self.description = description
        self.tags = [controllerType.specControllerName]
        self.responses = responses
        self.requestBody = requestBody
        self.parameters = parameters
        if let security = security {
            self.security = [[security: []]]
        }
        self.deprecated = deprecated
    }
}
