//
//  WhereActionKey.swift
//  App
//
//  Created by iLem0n on 11.06.20.
//

import Foundation

enum WhereActionKey: String {
    case like
    case gt
    case gte
    case lt
    case lte
}
