//
//  SpecSecuritySchema.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public enum SpecSecuritySchema {
    case jwt(String)
    case none
}

extension SpecSecuritySchema: Codable {
    
    enum BearerCodingKeys: String, CodingKey {
        case bearerFormat
        case type
        case `in`
        case description
        case scheme
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: BearerCodingKeys.self)
        
        let scheme = try container.decode(String.self, forKey: .scheme)
        let bearerFormat = try container.decode(String.self, forKey: .bearerFormat)
        let description = try container.decode(String.self, forKey: .description)
        
        if scheme == "bearer" && bearerFormat == "jwt" {
            self = .jwt(description)
        } else {
            self = .none
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        
        switch self {
        case .jwt(let description):
            var container = encoder.container(keyedBy: BearerCodingKeys.self)
            
            try container.encode("jwt", forKey: .bearerFormat)
            try container.encode("http", forKey: .type)
            try container.encode("header", forKey: .in)
            try container.encode(description, forKey: .description)
            try container.encode("bearer", forKey: .scheme)
        case .none:
            break
        }
    }
}
