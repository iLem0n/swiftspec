//
//  SpecServer.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public struct SpecServer: Codable {
    public let url: URL
    
    public init(url: URL) {
        self.url = url
    }
}
