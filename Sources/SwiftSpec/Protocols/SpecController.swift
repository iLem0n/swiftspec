//
//  SpecController.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public protocol SpecController {
    static var specControllerName: String { get }
    static var specControllerSchemas: [String: SpecDef] { get }
    
    static var specControllerDescription: String { get }
    static var specControllerSchemaEntities: [SpecEntity.Type] { get }
    static var specControllerSchemaEnums: [SpecEnum.Type] { get }
    static var specControllerSchemaPaths: [String: SpecPath] { get }
    
}

extension SpecController {
    public static var specControllerName: String { return "\(self)" }
    public static var specControllerSchemas: [String: SpecDef] {
        var schemas: [String: SpecDef] = [:]
        for schema in self.specControllerSchemaEntities {
            schemas[schema.schemaName] = schema.schema
        }
        for `enum` in self.specControllerSchemaEnums {
            schemas[`enum`.schemaName] = `enum`.schema
        }
        return schemas
    }
    public static var specControllerSchemaEnums: [SpecEnum.Type] { [] }
}
