//
//  SpecParameter.swift
//  SwiftSpec
//
//  Created by iLem0n on 25.07.20.
//

import Foundation

public enum SpecParameterLocation: String, Codable {
    case path
    case header
    case query
    case cookie
}

public enum SpecParameterDataType: String, Codable {
    case string
    case int
    case double
    case bool
}

public struct SpecParameter: Codable {
    public let name: String
    public let `in`: SpecParameterLocation
    public let required: Bool
    public let schema: SpecProperty
    
    public init(
        name: String,
        parameterLocation: SpecParameterLocation,
        required: Bool,
        schema: SpecProperty
    ) {
        self.name = name
        self.in = parameterLocation
        self.required = required
        self.schema = schema
    }
}
