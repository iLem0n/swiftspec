import Foundation

public struct Spec: Codable {
    public let openapi: String = "3.0.1"
    public let info: SpecInfo
    public let servers: [SpecServer]
    
    public private(set) var tags: [SpecTag] = []
    public private(set) var paths: [String: SpecPath] = [:]
    public private(set) var components: SpecComponents = SpecComponents()
    
    public init(
        info: SpecInfo,
        servers: [SpecServer],
        controllerTypes: [SpecController.Type],
        securitySchemes: [String: SpecSecuritySchema],
        additionalSchemes: [SpecEntity.Type]? = []
    ) throws {
        self.info = info
        self.servers = servers
                        
        for controllerType in controllerTypes {
            try addTags(for: controllerType)
            try addPaths(for: controllerType)
            for (key, schema) in controllerType.specControllerSchemas {
                if let schema = schema as? SpecSchemaDef {
                    try components.add(schema, forKey: key)
                }
                if let schema = schema as? SpecEnumDef {
                    try components.add(schema, forKey: key)
                }                
            } 
        }
        
        for (key, schema) in securitySchemes {
            try self.components.addSecurityScheme(schema, forKey: key)
        }
        
        if let schemes = additionalSchemes {
            for (schema) in schemes {
                try components.add(schema.schema, forKey: schema.schemaName)
            }
        }
    }
    
    mutating private func addTags(for controllerType: SpecController.Type) throws {
        let tag = SpecTag(from: controllerType)
        guard !self.tags.map({ $0.name }).contains(tag.name) else {
            throw SpecError.duplicateTagName(tag.name)
        }
        self.tags.append(tag)
    }
    
    mutating private func addPaths(for controllerType: SpecController.Type) throws {
        for path in controllerType.specControllerSchemaPaths.keys {
            guard !self.paths.keys.contains(path) else {
                throw SpecError.duplicateSchemaKey(path)
            }
            
            self.paths[path] = controllerType.specControllerSchemaPaths[path]
        }
    }
    
    public func asString() throws -> String? {

        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
                
        let data = try encoder.encode(self)
        return String(data: data, encoding: String.Encoding.utf8)!
    }
    
}
