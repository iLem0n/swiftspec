//
//  SpecTag.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public struct SpecTag: Codable {
    public var name: String
    public var description: String
    
    public init(from controller: SpecController.Type) {
        self.name = controller.specControllerName
        self.description = controller.specControllerDescription
    }
}
