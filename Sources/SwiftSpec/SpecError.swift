//
//  SpecError.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public enum SpecError: Error {
    case invalidProperty
    case invalidFormat(String, String)
    case duplicateSchemaKey(String)
    case duplicateSecuritySchemeKey(String)
    case duplicateTagName(String)
    case unexpectedPropertyType(String, String)
}
