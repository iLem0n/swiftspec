//
//  SpecContact.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public struct SpecContact: Codable {
    public var name: String
    public var email: String
    
    public init(name: String, email: String) {
        self.name = name
        self.email = email
    }
}
