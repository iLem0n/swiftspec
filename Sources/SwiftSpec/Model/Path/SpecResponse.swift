//
//  SpecResponse.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public struct SpecResponse: Codable {
    public let description: String
    public let content: SpecContentContainer?
    
    public init(
        description: String,
        content: SpecContentContainer? = nil
    ) {
        self.description = description
        self.content = content
    }
}
