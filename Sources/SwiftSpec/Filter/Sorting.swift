//
//  Sorting.swift
//  App
//
//  Created by iLem0n on 20.06.20.
//

enum SortingDecoderError: Error {
    case invalidFormat
}

public enum SortingDirection: String, Codable {
    case ascending = "ASC"
    case descending = "DESC"
    
    static var all: [SortingDirection] {
        [
            .ascending,
            .descending
        ]
    }
}

public struct Sorting: Codable {
    public var key: String
    public var direction: SortingDirection
    
    public init(from decoder: Decoder) throws {
        let string = try decoder.singleValueContainer().decode(String.self)
        let parts: [String] = string.split(separator: " ").map({ String($0) })
        
        guard let direction = SortingDirection(rawValue: parts[1])
        else { throw SortingDecoderError.invalidFormat }
        
        self.key = parts[0]
        self.direction = direction
    }
    
    init(key: String, direction: SortingDirection) {
        self.key = key
        self.direction = direction
    }
}
