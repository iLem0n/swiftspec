//
//  SpecRequestBody.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public struct SpecRequestBody: Codable {
    public let content: SpecContentContainer
    public let required: Bool
    
    public init(
        content: SpecContentContainer,
        required: Bool = false
    ) {
        self.content = content
        self.required = required
    }
}
