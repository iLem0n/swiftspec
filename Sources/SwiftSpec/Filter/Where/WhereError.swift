//
//  WhereError.swift
//  App
//
//  Created by iLem0n on 11.06.20.
//

import Foundation

enum WhereError: Error {
    case unhandledActionKey(String)
    case invalidStructure
    case notImplemented
}
