//
//  SpecInfo.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

public struct SpecInfo: Codable {
    public var title: String
    public var description: String
    public var version: String
    public var contact: SpecContact
    public var license: SpecLicense
   
    public init(
        title: String,
        description: String,
        version: String,
        contact: SpecContact,
        license: SpecLicense
    ) {
        self.title = title
        self.description = description
        self.version = version
        self.contact = contact
        self.license = license
    }
}
