//
//  SpecProperty.swift
//  SwiftSpec
//
//  Created by iLem0n on 24.07.20.
//

import Foundation

indirect public enum SpecProperty {
    case uuid(Bool)
    case ref(SpecEntity.Type, Bool)
    case refString(String, Bool)
    case string(String?, Bool)
    case int(Int?, Bool)
    case double(Double?, Bool)
    case bool(Bool)
    case array(SpecProperty, Bool)
    case file(Bool)
}

extension SpecProperty: Codable {
    enum RefCodingKeys: String, CodingKey {
        case ref = "$ref"
        case required
    }
    
    enum TypeCodingKeys: String, CodingKey {
        case type
        case example
        case required
        case items
        case properties
    }
    
    public init(from decoder: Decoder) throws {
        let refContainer = try decoder.container(keyedBy: RefCodingKeys.self)
        let typeContainer = try decoder.container(keyedBy: TypeCodingKeys.self)
        
        if refContainer.contains(.ref) {
            let ref = try refContainer.decode(String.self, forKey: .ref)
            let required = try? typeContainer.decode(Bool.self, forKey: .required)
            if ref == "#/components/schemas/UUID" {
                self = .uuid(required ?? false)
            } else {
                self = .refString(ref, required ?? false)
            }
        } else if typeContainer.contains(.type) {
            let type = try typeContainer.decode(String.self, forKey: .type)
            let required = try? typeContainer.decode(Bool.self, forKey: .required)

            switch type {
            case "string":
                let example = try? typeContainer.decode(String.self, forKey: .example)
                self = .string(example, required ?? false)
            case "integer":
                let example = try? typeContainer.decode(Int.self, forKey: .example)
                self = .int(example, required ?? false)
            case "double":
                let example = try? typeContainer.decode(Double.self, forKey: .example)
                self = .double(example, required ?? false)
            case "boolean":
                self = .bool(required ?? false)
            case "array":
                let items = try typeContainer.decode(SpecProperty.self, forKey: .items)
                self = .array(items, required ?? false)
            default:
                throw SpecError.invalidProperty
            }
        } else {
            throw SpecError.invalidProperty
        }
    }
       
    
    public func encode(to encoder: Encoder) throws {
        var refContainer = encoder.container(keyedBy: RefCodingKeys.self)
        var typeContainer = encoder.container(keyedBy: TypeCodingKeys.self)

        switch self {
        case .refString(let ref, _):
            try refContainer.encode(ref, forKey: .ref)
            
        case .ref(let entityType, _):
            try refContainer.encode("#/components/schemas/\(entityType.schemaName)", forKey: .ref)

        case .uuid(_):
            try refContainer.encode("#/components/schemas/UUID", forKey: .ref)
            
        case .string(let example, _):
            try typeContainer.encode("string", forKey: .type)
            
            if let example = example {
                try typeContainer.encode(example, forKey: .example)
            }
            
        case .int(let example, _):
            try typeContainer.encode("number", forKey: .type)
                      
            if let example = example {
                try typeContainer.encode(example, forKey: .example)
            }
            
        case .double(let example, _):
            try typeContainer.encode("number", forKey: .type)
                                  
            if let example = example {
                try typeContainer.encode(example, forKey: .example)
            }
            
        case .bool(_):
            try typeContainer.encode("boolean", forKey: .type)
                                
        case .array(let propertyType, _):
            try typeContainer.encode("array", forKey: .type)
            try typeContainer.encode(propertyType, forKey: .items)
            
        case .file(_):
            try typeContainer.encode("object", forKey: .type)
            try typeContainer.encode(FileProperties(), forKey: .properties)
        }
            
    }
}

private struct FileProperties: Codable {
    struct FileType: Codable {
        let type = "string"
        let format = "binary"
    }
    
    let data = FileType()
    
    init() {}
}
