import Cocoa
import SwiftSpec

extension Data {
    var prettyPrintedJSONString: NSString? { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }

        return prettyPrintedString
    }
}

class ExampleEntity: SpecEntity {
    var name: String = "Tom"
    var age: Int = 30
    var budget: Double = 123.45
    var isAdmin: Bool = false
    
    static var schemaProperties: [String : SpecProperty] {
        [
            "name": .string(nil, false),
            "age": .int(nil, false),
            "budget": .double(nil, false),
            "isAdmin": .bool(false),
            "children": .array(.ref(ExampleEntity.self, true), true)
        ]
    }
}

enum ExampleEnum: String, CaseIterable, SpecEnum {
    case one
    case two
    case three
}

class AdditionalEntity: SpecEntity {
    var name: String = "Tom"
    var age: Int = 30
    var budget: Double = 123.45
    var isAdmin: Bool = false
    
    static var schemaProperties: [String : SpecProperty] {
        [
            "name": .string(nil, false),
            "age": .int(nil, false),
            "budget": .double(nil, false),
            "isAdmin": .bool(false),
            "children": .array(.ref(ExampleEntity.self, true), false)
        ]
    }
}


class ExampleController: SpecController {
    static var specControllerDescription: String {
        "Example Controller"
    }
    
    static var specControllerSchemaEntities: [SpecEntity.Type] {
        [
            ExampleEntity.self,            
        ]
    }
    
    static var specControllerSchemaEnums: [SpecEnum.Type] {
        [
            ExampleEnum.self
        ]
    }
    
    static var specControllerSchemaPaths: [String : SpecPath] {
        [
            "/example/{exampleId}": SpecPath(
                parameters: [
                    SpecParameter(name: "exampleId", parameterLocation: .path, required: true, schema: .string("Test", true))
                ],
                get: SpecOperation(
                    operationId: "getExample",
                    summary: "Example GET",
                    description: "Example GET Description",
                    controllerType: Self.self,
                    responses: [
                        "200": SpecResponse(
                            description: "REsponse description",
                            content: SpecContentContainer(json: .schema(.ref(ExampleEntity.self, true)))
                        ),
                        "201": SpecResponse(
                            description: "Response description",
                            content: SpecContentContainer(json: .schema(.array(.ref(ExampleEntity.self, true), true)))
                        )
                    ],
                    security: "auth_jwt"
                )
            )
        ]
    }
}

let contact = SpecContact(name: "Tom Test", email: "tom@test.de")
let license = SpecLicense(name: "MIT", url: URL(string: "mit.license")!)
let info = SpecInfo(
    title: "Test API",
    description: "API for testing SwiftSpec",
    version: "1.0.0",
    contact: contact,
    license: license
)
let server = SpecServer(url: URL(string: "http://localhost:8080")!)
let spec = try Spec(
    info: info,
    servers: [
        server
    ],
    controllerTypes: [ExampleController.self],
    securitySchemes: ["auth_jwt": .jwt("JWT Auth")],
    additionalSchemes: [AdditionalEntity.self]
)


let str = try spec.asString()
debugPrint(str?.data(using: .utf8)?.prettyPrintedJSONString)

